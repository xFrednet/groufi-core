use diesel::prelude::*;
use uuid::Uuid;

use crate::models;
use models::CreateUserInfo;

// TODO check for usernames with all lowercase 
// TODO on creation make sure the username doesn't exist in the database... well this is difficult do to race conditions... I'll think of something

pub fn create_new_user_info(
    new_user: CreateUserInfo,
    conn: &PgConnection
) -> Result<models::UserInfo, diesel::result::Error> {
    use crate::schema::user_info;

    diesel::insert_into(user_info::table)
        .values(&new_user)
        .get_result(conn)
}

pub fn load_user_info_by_uuid(
    uuid: &Uuid,
    conn: &PgConnection
) -> Result<Option<models::UserInfo>, diesel::result::Error> {
    use crate::schema::user_info::dsl::*;

    let user = user_info
        .filter(id.eq(uuid))
        .first::<models::UserInfo>(conn)
        .optional()?;

    Ok(user)
}

pub fn load_user_info_by_email(
    search_email: &str,
    conn: &PgConnection
) -> Result<Option<models::UserInfo>, diesel::result::Error> {
    use crate::schema::user_info::dsl::*;

    let user = user_info
        .filter(email.eq(search_email))
        .first::<models::UserInfo>(conn)
        .optional()?;

    Ok(user)
}