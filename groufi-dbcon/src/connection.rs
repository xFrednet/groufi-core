use actix::prelude::*;

use diesel::PgConnection;
use diesel::r2d2::{self, ConnectionManager};

pub type DbPool = r2d2::Pool<ConnectionManager<PgConnection>>;

pub struct DbCon(PgConnection);

impl Actor for DbCon {
    type Context = SyncContext<Self>;
}

pub fn create_pool(database_url: String) -> DbPool {
    let manager = ConnectionManager::<PgConnection>::new(database_url);
    diesel::r2d2::Pool::builder()
        .build(manager).unwrap()
}
