use crate::schema::user_info;

use uuid::Uuid;

use validator::Validate;

#[derive(Debug, Clone, Builder, Queryable)]
pub struct UserInfo {
    pub id: Uuid,
    pub email: String,

    pub first_name: String,
    pub last_name: String,
    pub display_name: String,
    
    pub description: String
    // Profile image ?
}

#[derive(Insertable, Clone, Validate, Deserialize)]
#[table_name="user_info"]
pub struct CreateUserInfo {
    #[validate(email(message="validation.invalidEmailSyntax"))]
    #[validate(regex(path="crate::validation::RE_DATA_STRING", message="validation.invalidDataString"))]
    #[validate(length(max=256, message="validation.stringMaxLength"))]
    pub email: String,

    #[validate(regex(path="crate::validation::RE_DATA_STRING", message="validation.invalidDataString"))]
    #[validate(length(max=100, message="validation.stringMaxLength"))]
    #[serde(rename = "firstName")]
    pub first_name: String,
    
    #[validate(regex(path="crate::validation::RE_DATA_STRING", message="validation.invalidDataString"))]
    #[validate(length(max=100, message="validation.stringMaxLength"))]
    #[serde(rename = "lastName")]
    pub last_name: String,
    
    #[validate(regex(path="crate::validation::RE_DATA_STRING", message="validation.invalidDataString"))]
    #[validate(length(min=3, max=100, message="validation.stringMinMaxLength"))]
    #[serde(rename = "displayName")]
    pub display_name: String,
    
    #[validate(regex(path="crate::validation::RE_DESCRIPTION", message="validation.invalidDescription"))]
    #[validate(length(max=1024, message="validation.stringMaxLength"))]
    pub description: String
}