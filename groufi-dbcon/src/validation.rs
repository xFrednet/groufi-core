
use regex::Regex;

lazy_static! {
    // A string with no line break and a visible char at the start and end.
    pub static ref RE_DATA_STRING: Regex = Regex::new(r"^(\S.*\S|\S)$").unwrap();

    // // A string with no invisible and special characters like /)(=#+*/-) underscores are allowed
    // pub static ref RE_DISPLAY_NAME: Regex = Regex::new(r"^(\w|ö|ü|ä|ß| )*$").unwrap();

    // A string with visible chars at the start and end of the sting (line breaks are allowed)
    pub static ref RE_DESCRIPTION: Regex = Regex::new(r"^(\S(.|\W)*\S|\S)*$").unwrap();
}