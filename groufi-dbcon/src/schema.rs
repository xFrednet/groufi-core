table! {
    user_info (id) {
        id -> Uuid,
        email -> Varchar,
        first_name -> Varchar,
        last_name -> Varchar,
        display_name -> Varchar,
        description -> Varchar,
    }
}
