mod connection;
pub use connection::DbPool;
pub use connection::create_pool;

pub(crate) mod validation;

pub(crate) mod schema;

pub mod models;

pub mod actions;

// database
#[macro_use]
extern crate diesel;

// utility
#[macro_use]
extern crate derive_builder;
#[macro_use]
extern crate lazy_static;

// validation
#[macro_use]
extern crate validator_derive;

// serialization
#[macro_use]
extern crate serde_derive;