extern crate groufi_dbcon;
extern crate rand;
extern crate validator;

use groufi_dbcon::models::CreateUserInfo;
use validator::Validate;
use rand::Rng;

fn create_valid_create_user_info() -> CreateUserInfo {
    CreateUserInfo {
        email: String::from("ferris@rust-lang.org"),
        first_name: String::from("Ferris"),
        last_name: String::from("the crab"),
        display_name: String::from("Ferris the Crab"),
        description: String::from("This is Ferris the rusty crab")
    }
}

fn create_rand_string(len: usize) -> String {
    const CHARSET: &[u8] = b"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    let mut rng = rand::thread_rng();
    
    (0..len).map(|_| {
        let index = rng.gen_range(0, CHARSET.len());
        CHARSET[index] as char
    })
    .collect()
}

#[test]
fn test_user_creation_email_validation() {
    let mut user = create_valid_create_user_info();
    assert_eq!(Ok(()), user.validate());

    // correct
    user.email = String::from("test@test.com");
    assert!(user.validate().is_ok());
    user.email = String::from("address@something.domain");
    assert!(user.validate().is_ok());
    user.email = String::from("address@something.domain.long.name");
    assert!(user.validate().is_ok());

    // invalid syntax
    user.email = String::from("");
    assert!(user.validate().is_err());
    user.email = String::from("no.at");
    assert!(user.validate().is_err());
    user.email = String::from("@at.at.the.start");
    assert!(user.validate().is_err());
    user.email = String::from("at.at.the.end@");
    assert!(user.validate().is_err());
    
    // length
    let long_string = create_rand_string(250);
    user.email = String::from("a@b.c");
    assert!(user.validate().is_ok());
    user.email = long_string.clone() + "a@ab.c";
    assert!(user.validate().is_ok());
    user.email = long_string.clone() + "a@ab.cd";
    assert!(user.validate().is_err());
}

#[test]
fn test_user_creation_first_name_validation() {
    let mut user = create_valid_create_user_info();
    assert_eq!(Ok(()), user.validate());

    user.first_name = String::from("Ferris");
    assert!(user.validate().is_ok());
    user.first_name = create_rand_string(99);
    assert!(user.validate().is_ok());
    user.first_name = create_rand_string(101);
    assert!(user.validate().is_err());
}

#[test]
fn test_user_creation_last_name_validation() {
    let mut user = create_valid_create_user_info();
    assert_eq!(Ok(()), user.validate());

    user.last_name = String::from("Crazy Canny");
    assert!(user.validate().is_ok());
    user.last_name = create_rand_string(99);
    assert!(user.validate().is_ok());
    user.last_name = create_rand_string(101);
    assert!(user.validate().is_err());
}


#[test]
fn test_user_creation_username_validation() {
    let mut user = create_valid_create_user_info();
    assert_eq!(Ok(()), user.validate());
    
    // to short
    user.display_name = String::from("");
    assert!(user.validate().is_err());
    
    // correct
    user.display_name = String::from("Fred");
    assert!(user.validate().is_ok());
    user.display_name = String::from("Ferris the Crab");
    assert!(user.validate().is_ok());
    user.display_name = String::from("Le King the Rust");
    assert!(user.validate().is_ok());
    user.display_name = String::from("12345");
    assert!(user.validate().is_ok());
    user.display_name = String::from("xXSuperMasterXx");
    assert!(user.validate().is_ok());
    user.display_name = create_rand_string(99);
    assert!(user.validate().is_ok());

    // to longcreate_string_with_length
    user.display_name = create_rand_string(101);
    assert!(user.validate().is_err());
    user.display_name = create_rand_string(256);
    assert!(user.validate().is_err());
}

#[test]
fn test_user_creation_description_validation() {
    let mut user = create_valid_create_user_info();
    assert_eq!(Ok(()), user.validate());

    user.description = String::from(r#"#Duck
    Hello World
    ### Everything is awesome"#);
    assert!(user.validate().is_ok());
    user.description = create_rand_string(99);
    assert!(user.validate().is_ok());
}