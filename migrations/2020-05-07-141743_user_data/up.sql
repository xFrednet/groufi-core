-- Your SQL goes here
create table USER_INFO (
    ID uuid primary key not null default uuid_generate_v1(),
    EMAIL varchar(256) not null,
    
    FIRST_NAME varchar(100) not null,
    LAST_NAME varchar(100) not null,
    DISPLAY_NAME varchar(100) not null,
    
    DESCRIPTION varchar(1024) not null,

    unique (EMAIL)
);