FROM rust:1.43 as builder

RUN cargo install diesel_cli

WORKDIR /usr/src/groufi-core
COPY . .

RUN cargo install --path .


FROM debian:buster-slim

# TODO xFrednet 07.05.2020: Add groufi-core dependencies

COPY --from=builder /usr/local/cargo/bin/groufi-core /usr/local/bin/groufi-core

CMD ["groufi-core"]