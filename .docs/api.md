
API V1:
 
# `/api/v1/`

## `/user/`

### POST `/user/new`

This request accepts new user information and returns the created user.

**Example body data:**

```json
{
    "email": "ferris@rust-lang.org",
    "firstName": "Ferris",
    "lastName": "The Crab",
    "displayName": "Ferris the Crab",
    "description": "# Hello I'm ferris the crab!"
}
```

### GET `/user/{uuid}`

Retrieves a user by its uuid.

**Example:**

 * `GET /api/v1/user/d7ee12e2-95c2-11ea-b51b-0242ac130002`

```json
{
    "id": "d7ee12e2-95c2-11ea-b51b-0242ac130002",
    "email": "ferris@rust-lang.org",
    "firstName": "Ferris",
    "lastName": "The Crab",
    "displayName": "Ferris the Crab",
    "description": "# Hello I'm ferris the crab!"
}
```


## `/event`

### GET | Searches the database
 | Params | Type | Description | Required |
 |---|---|---|:---:|
 | `lati` | float | The latitude from were to search from | [x] |
 | `long` | float | The longitude from were to search from | [x] |
 | `range` | float | The range in km from the given location | [x] |
 | `minDate` | long | The earliest date to be searched for | [x] |
 | `maxDate` | long | The latest date to be searched for | [ ] |
 | `freeSpace` | int | The number of free spaces that should at least be available | [ ] |
 | `search` | string | Comma separated text that should be searched for¹ | [ ] |
 | `tags` | string | Comma separated tags that should be searched for¹ | [ ] |
 
 ¹ A minus can be used to blacklist a search term.
 
**Example:**
 1. `GET /event?lati=1.2345&long=6.7890&range=4.5&minDate=0`
 2. `GET /event?lati=1.2345&long=6.7890&range=4.5&minDate=0&tags=CONFERENCE,-MUSIC`



### `/event/<id>/date/`
