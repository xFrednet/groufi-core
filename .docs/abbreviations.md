# Code abbreviations

 | abbreviation | Long term | Description |
 |---|---|---|
 | DAO | data access object | is a pattern that provides an abstract interface to some type of database or other persistence mechanism |
 | DTO | data transfer object | is an object that carries data between processes |
