# Groufi-core the core of your friendly group finder

This is the rust core for the app. It uses docker!

(I should update this description later on... let's put a TODO here xD ~xFrednet)

## Setup

This will guide you (and probably mostly me) through the initial setup :)

### Docker

 1. Create a docker volume for the database:

    `$ docker volume create groufi-postgres`
