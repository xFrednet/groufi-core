use groufi_dbcon::{actions, DbPool, models};

use actix_web::{web, HttpResponse, error::BlockingError};

use serde::{Serialize, Deserialize};

// database
use diesel::result::*;
use uuid::Uuid;

// validation
use validator::Validate;
use validator::ValidationError;
use validator::ValidationErrors;

#[derive(Serialize, Deserialize)]
pub struct UserInfoDto {
    id: Uuid,
    email: Option<String>,
    
    #[serde(rename = "firstName")]
    first_name: Option<String>,
    #[serde(rename = "lastName")]
    last_name: Option<String>,
    #[serde(rename = "displayName")]
    display_name: String,

    description: String
}

pub(crate) async fn create_user(
    mut creation_data: web::Json<models::CreateUserInfo>,
    pool: web::Data<DbPool>
) -> Result<web::Json<UserInfoDto>, HttpResponse> {
    
    creation_data.email = creation_data.email.to_lowercase();

    let validation = creation_data.validate();
    if validation.is_err() {
        let err : ValidationErrors = validation.unwrap_err();
        return Err(HttpResponse::BadRequest().json(err));
    };

    let conn = pool.get().expect("couldn't get db connection from pool");

    // use web::block to offload blocking Diesel code without blocking server thread
    let user = web::block(move || actions::create_new_user_info(creation_data.into_inner(), &conn))
        .await
        .map(|user| {
            UserInfoDto {
                id: user.id,
                email: Some(user.email),
                first_name: Some(user.first_name),
                last_name: Some(user.last_name),
                display_name: user.display_name,
                description: user.description
            }
        })
        .map_err(|blocking_error| {
            match blocking_error {
                BlockingError::Error(Error::DatabaseError(DatabaseErrorKind::UniqueViolation, _)) => {
                    HttpResponse::BadRequest().json(ValidationError::new("validation.emailUniqueViolation"))
                },
                error => {
                    println!("{:?}", error);
                    HttpResponse::InternalServerError().finish()
                }
            }
        })?;

    Ok(web::Json(user))
}

pub(crate) async fn get_user(
    uuid: web::Path<Uuid>,
    pool: web::Data<DbPool>
) -> Result<web::Json<UserInfoDto>, HttpResponse> {
    
    let conn = pool.get().expect("couldn't get db connection from pool");

    let info_username = *uuid;
    // use web::block to offload blocking Diesel code without blocking server thread
    let user = web::block(move || actions::load_user_info_by_uuid(uuid.as_ref(), &conn))
        .await
        .map_err(|e| {
            eprintln!("{}", e);
            HttpResponse::InternalServerError().finish()
        })?;
    
    if user.is_some() {
        let user = user.unwrap();

        let user_dto = UserInfoDto {
            id: user.id,
            email: Some(user.email),
            first_name: Some(user.first_name),
            last_name: Some(user.last_name),
            display_name: user.display_name,
            description: user.description
        };

        Ok(web::Json(user_dto))
    } else {
        let res = HttpResponse::NotFound()
            .body(format!("No entity with the username: {}", info_username));
        Err(res)
    }
}

pub(crate) async fn get_user_by_email(
    web::Query(email): web::Query<String>,
    pool: web::Data<DbPool>
) -> Result<web::Json<UserInfoDto>, HttpResponse> {
    
    let conn = pool.get().expect("couldn't get db connection from pool");

    let info_username = email.clone();
    // use web::block to offload blocking Diesel code without blocking server thread
    let user = web::block(move || actions::load_user_info_by_email(email.to_lowercase().as_ref(), &conn))
        .await
        .map_err(|e| {
            eprintln!("{}", e);
            HttpResponse::InternalServerError().finish()
        })?;
    
    if user.is_some() {
        let user = user.unwrap();

        let user_dto = UserInfoDto {
            id: user.id,
            email: Some(user.email),
            first_name: Some(user.first_name),
            last_name: Some(user.last_name),
            display_name: user.display_name,
            description: user.description
        };

        Ok(web::Json(user_dto))
    } else {
        let res = HttpResponse::NotFound()
            .body(format!("No entity with the username: {}", info_username));
        Err(res)
    }
}