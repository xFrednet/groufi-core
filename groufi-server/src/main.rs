mod api;

// .env file
#[macro_use]
extern crate dotenv_codegen;
use dotenv::dotenv;

// server
use actix_web::{web, middleware, App, HttpResponse, HttpServer, Responder};

// validation
#[macro_use]
extern crate validator_derive;

// util
#[macro_use]
extern crate derive_builder;

async fn hello() -> impl Responder {
    HttpResponse::Ok().body("Hello World")
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();
    env_logger::init();

    let db_url = std::env::var("DATABASE_URL").expect("Missing DATABASE_URL environment value");

    HttpServer::new(move || {
        App::new()
            // enable logger - always register actix-web Logger middleware last
            .data(groufi_dbcon::create_pool(db_url.clone()))
            .wrap(middleware::Logger::default())
            .route("/", web::get().to(hello))
            .route("/api/v1/user/{uuid}", web::get().to(api::user::get_user))
            .route("/api/v1/user", web::get().to(api::user::get_user_by_email))
            .route("/api/v1/user/new", web::post().to(api::user::create_user))
    })
    .bind( dotenv!("SERVER_BIND"))?
    .run()
    .await
}